@extends('master')

@section('content')
<section>
	<div class="container">
		<div class="row">
			@include('components.categories')
		</div>
	</div>
</section>
@endsection