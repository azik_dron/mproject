@foreach(App\Category::all() as $categories)
    <div class="col-xs-12 col-xs-6 col-sm-4 col-md-3">
        <a href="/categories/{{$categories->id}}" class="thumbnail text-center" >
            <span><i class="{{$categories->image_icon}}" aria-hidden="true"></i></span>
            <h4>{{$categories->category_name}}</h4>
        </a>
    </div>
@endforeach