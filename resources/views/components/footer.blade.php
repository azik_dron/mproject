<link href="https://fortawesome.github.io/Font-Awesome/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
<!--footer start from here-->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 footerleft ">
                <div class="logofooter"> Logo</div>
                <p><i class="fa fa-map-pin"></i> КГУСТА </p>
                <p><i class="fa fa-phone"></i> Телефон (Кыргызстан) : +996 7** *** ***</p>
                <p><i class="fa fa-envelope"></i> E-mail : testKGFI@gmail.com</p>

            </div>
            <div class="col-md-2 col-sm-6 paddingtop-bottom">
                <h6 class="heading7">Компания</h6>
                <ul class="footer-ul">
                    <li><a href="{{ url('about')}}"> О нас</a></li>
                    <li><a href="#"> Вакансии</a></li>
                    <li><a href="{{url('blog')}}"> Блог</a></li>
                    <li><a href="#"> Контакты</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 paddingtop-bottom">
                <h6 class="heading7">Заказчикам</h6>
                <ul class="footer-ul">
                    <li><a href="#"> Создать задачу</a></li>
                    <li><a href="{{url('register')}}"> Регистрация</a></li>
                    <li><a href="#"> Оплата</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-sm-6 paddingtop-bottom">
                <div class="fb-page" data-href="https://www.facebook.com/facebook" data-tabs="timeline" data-height="300" data-small-header="false" style="margin-bottom:15px;" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">
                    <div class="fb-xfbml-parse-ignore">
                        <blockquote>
                            <a href="https://www.facebook.com/bootsnipp"><i id="social-fb" class="fa fa-facebook-square fa-3x social"></i></a>
                            <a href="https://twitter.com/bootsnipp"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                            <a href="https://plus.google.com/+Bootsnipp-page"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                            <a href="mailto:bootsnipp@gmail.com"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                        </blockquote>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!--footer start from here-->

<div class="copyright">
    <div class="container">
        <div class="col-md-6">
            <p>© 2017 - All Rights</p>
        </div>
    </div>
</div>