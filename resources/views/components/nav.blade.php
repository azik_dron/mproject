<nav class="navbar navbar-default" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="navbar-collapse style= collapse in" id="bs-megadropdown-tabs" style="
    padding-left: 0px;
">
        <ul class="nav navbar-nav">
            <li><a href="/"> Главная</a></li>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Категории <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>@include('components.dropdowncat')</li>
                </ul>
            </li>
            <li><a href="{{ url('about')}}"> О нас</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
        <li><a href="{{ url('login')}}"> Вход</a></li>
        </ul>
        <form class="navbar-form navbar-right" role="search">
            <div class="form-group">
              <input type="text" class="form-control" placeholder="Search">
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>