<!DOCTYPE html>
<html>
<head>
	<meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<title></title>
</head>
<body>
	@include('components.nav')
		<div>
			@yield('content')
		</div>
	@include('components.footer')
</body>
<script type="text/javascript">
	$(document).ready(function(){
    $(".dropdown").hover(            
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideDown("fast");
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).stop( true, true ).slideUp("fast");
            $(this).toggleClass('open');       
        }
    );
});

$(document).ready( function() {
    $('#myCarousel').carousel({
        interval:   4000
	});
	
	var clickEvent = false;
	$('#myCarousel').on('click', '.nav a', function() {
			clickEvent = true;
			$('.nav li').removeClass('active');
			$(this).parent().addClass('active');		
	}).on('slid.bs.carousel', function(e) {
		if(!clickEvent) {
			var count = $('.nav').children().length -1;
			var current = $('.nav li.active');
			current.removeClass('active').next().addClass('active');
			var id = parseInt(current.data('slide-to'));
			if(count == id) {
				$('.nav li').first().addClass('active');	
			}
		}
		clickEvent = false;
	});
});
</script>
<style type="text/css">

#login-dp{
    min-width: 250px;
    padding: 14px 14px 0;
    overflow:hidden;
    background-color:rgba(255,255,255,.8);
}
#login-dp .help-block{
    font-size:12px    
}
#login-dp .bottom{
    background-color:rgba(255,255,255,.8);
    border-top:1px solid #ddd;
    clear:both;
    padding:14px;
}
#login-dp .social-buttons{
    margin:12px 0    
}
#login-dp .social-buttons a{
    width: 49%;
}
#login-dp .form-group {
    margin-bottom: 10px;
}	
.btn-fb{
    color: #fff;
    background-color:#3b5998;
}
.btn-fb:hover{
    color: #fff;
    background-color:#496ebc 
}
.btn-tw{
    color: #fff;
    background-color:#55acee;
}
.btn-tw:hover{
    color: #fff;
    background-color:#59b5fa;
}
    .service-wrapper {
  background: #FFF;
  margin: 70px 10px;
  text-align: center;
  padding: 30px 20px;
  -webkit-border-radius: 5px;
  -webkit-background-clip: padding-box;
  -moz-border-radius: 5px;  
  -moz-background-clip: padding;
  border-radius: 5px;
  background-clip: padding-box;
  -webkit-box-shadow: 0 0 3px #999;
  -moz-box-shadow: 0 0 3px #999;
  box-shadow: 0 0 3px #999;
  color: 0 0 3px #999;
}
.service-wrapper h3 {
  font-size: 1.2em;
  margin: 10px 0 !important;
}
.service-wrapper p {
  margin-top: 0;
}
/* Row */
.service-wrapper-row {
  padding: 10px 0;
}
.service-wrapper-row h3 {
  padding-top: 15px;
}
.service-wrapper-row .service-image {
  padding-top: 15px;
  text-align: center;
}
.service-wrapper-row .service-image img {
  max-width: 80%;
  vertical-align: bottom;
  bottom: 0;
  border: 7px solid #FFF;
  -webkit-border-radius: 5px;
  -webkit-background-clip: padding-box;
  -moz-border-radius: 5px;
  -moz-background-clip: padding;
  border-radius: 5px;
  background-clip: padding-box;
  -webkit-box-shadow: 0 0 8px #999;
  -moz-box-shadow: 0 0 8px #999;
  box-shadow: 0 0 8px #999;
  color: 0 0 8px #999;
}
  @import url(https://fonts.googleapis.com/css?family=Open+Sans:400,700,300);
    footer { background-color:#0c1a1e; min-height:350px; font-family: 'Open Sans', sans-serif; }
    .footerleft { margin-top:50px; padding:0 36px; }
    .logofooter { margin-bottom:10px; font-size:25px; color:#fff; font-weight:700;}

    .footerleft p { color:#fff; font-size:12px !important; font-family: 'Open Sans', sans-serif; margin-bottom:15px;}
    .footerleft p i { width:20px; color:#999;}


    .paddingtop-bottom {  margin-top:50px;}
    .footer-ul { list-style-type:none;  padding-left:0px; margin-left:2px;}
    .footer-ul li { line-height:29px; font-size:12px;}
    .footer-ul li a { color:#a0a3a4; transition: color 0.2s linear 0s, background 0.2s linear 0s; }
    .footer-ul i { margin-right:10px;}
    .footer-ul li a:hover {transition: color 0.2s linear 0s, background 0.2s linear 0s; color:#ff670f; }

    .social:hover {
        -webkit-transform: scale(1.1);
        -moz-transform: scale(1.1);
        -o-transform: scale(1.1);
    }

    .icon-ul { list-style-type:none !important; margin:0px; padding:0px;}
    .icon-ul li { line-height:75px; width:100%; float:left;}
    .icon { float:left; margin-right:5px;}


    .copyright { min-height:40px; background-color:#000000;}
    .copyright p { text-align:left; color:#FFF; padding:10px 0; margin-bottom:0px;}
    .heading7 { font-size:21px; font-weight:700; color:#d9d6d6; margin-bottom:22px;}
    .post p { font-size:12px; color:#FFF; line-height:20px;}
    .post p span { display:block; color:#8f8f8f;}
    .bottom_ul { list-style-type:none; float:right; margin-bottom:0px;}
    .bottom_ul li { float:left; line-height:40px;}
    .bottom_ul li:after { content:"/"; color:#FFF; margin-right:8px; margin-left:8px;}
    .bottom_ul li a { color:#FFF;  font-size:12px;}
</style>
</html>