<?php

namespace App\Http\Controllers;

use App\Category;
use App\Posts;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function CategoryAction($id){
        $category = Category::find($id);
        if ($category){
//            $posts = Posts::all();
            $posts = Posts::find(['category_id' => $category->category_id]);
            var_dump($posts);
//            print_r($posts2);
            return view('posts', ['posts' => $posts]);
        }
    }
}
